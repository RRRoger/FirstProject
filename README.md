# FirstProject

# 笔记

## 0x00 琐碎

* [Roger's Gist](https://gist.github.com/RRRoger)
* [什么是递归?](notebook/trivial/recursion.md)
* [关闭pad和iPhone来电同步](notebook/trivial/关闭pad和iPhone来电同步.md)
* [mac os 之间屏幕共享](notebook/trivial/macos之间屏幕共享.md)
* [Cut Music](py-music/readme.md)
* [搜狗输入法自定义短语](other_script/sougou_input/phrases.ini)
* [linux和win定时任务实现](notebook/trivial/linux和win定时任务实现.md)
* [该不该提前还贷？](https://www.v2ex.com/t/531921)
* [解决Redmine看不到"新建问题"按钮的问题](http://www.redmine.org/boards/2/topics/46330)
* [Ubuntu下socks代理dante-server安装与配置](notebook/trivial/Ubuntu下socks代理dante-server安装与配置.md)
* [Ubuntu下socks代理dante-server安装与配置[Use Docker]](https://github.com/wernight/docker-dante)
* [Proxifier：Mac OS给某个单独应用配置代理上网服务](notebook/trivial/Proxifier使用.md)

## 0x01 linux

* [乱炖](notebook/linux/乱炖.md)
* [Ubuntu安装微软字体](notebook/linux/Ubuntu安装微软字体.md)

- [换源整合](notebook/linux/ubuntu换源)

## 0x02 odoo

* [Supervisor管理odoo服务](notebook/odoo/Supervisor管理odoo服务.md)
* [odoo接口返回值格式修改](notebook/odoo/odoo接口返回值格式修改.md)
* [Ubuntu源码安装Odoo10社区版](notebook/odoo/Ubuntu14.04源码安装Odoo10社区版.md)
* [one2many搜索去重实现](notebook/odoo/[odoo]one2many搜索去重实现.md)
* [添加自定义按钮](https://github.com/RRRoger/odoo_addons/tree/master/tree_view_button/readme.md)
* [xmlrpc Call Odoo Server](https://github.com/RRRoger/odoo_addons/tree/master/odoo_xmlrpc/README.md)
* [Data Analysis Report](https://github.com/RRRoger/odoo_addons/tree/master/hs_query)

## 0x03 小程序

*  [微信小程序支持多语言](notebook/weapp/微信小程序支持多语言.md)

## 0x04 数据库

### 1.postgres

* [sql优化](notebook/database/sql优化.md)
* [数据库迁移](notebook/database/postgres/数据库迁移.md)
* [数据库死锁查询](notebook/database/postgres/数据库死锁查询.md)
* [unnest - 数组集合转table](notebook/database/postgres/数组集合转table.md)
* [远程 - 允许远程访问配置修改](notebook/database/postgres/允许远程访问配置修改.md)
* [升级 - 数据库升级](notebook/database/postgres/数据库升级.md)
* [迁移 - pg数据库迁移,步骤](notebook/database/postgres/pg数据库迁移,步骤.md)
* [空间 - 看数据库大小等](notebook/database/postgres/看数据库大小等.md)
* [加密 - 数据库文件](notebook/database/postgres/数据库文件.md)
* [递归 - 可能会用到](notebook/database/postgres/可能会用到.md)
* [权限 - 修改数据库用户](notebook/database/postgres/修改数据库用户.md)
* [数据库备份恢复](notebook/database/postgres/数据库备份恢复.md)
* [字符串常用函数](notebook/database/postgres/字符串常用函数.md)

### 2.oracle

* [cx_Oracle安装](notebook/database/oracle/cx_Oracle安装.md)

### 3.sqlserver

## 0x05 rabbitmq

* [安装](notebook/rabbitmq/安装mq.md)
* [清除队列步骤](notebook/rabbitmq/清除队列步骤.md)

## 0x06 python2.x

* [安装ipython 和 jupyter-notebook](notebook/python/安装ipython&jupyter-notebook-python2.md)
* [解析配置文件](other_tools/parse_conf_file/解析配置文件.md)

## 0x07 Script

- [进制转化](notebook/trivial/进制转换.md)

## 0X08 视频剪辑笔记

- [Final_Cut_Pro_Tips](notebook/视频剪辑笔记/Final_Cut_Pro_Tips.md)
- [PS_Tips](notebook/视频剪辑笔记/PS_Tips.md)


